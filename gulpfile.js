var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var concat = require('gulp-concat');

gulp.task('default', ['sass', 'watch']);

gulp.task('sass', function(){
	return gulp.src('./src/*.scss')
	.pipe(sass())
	.pipe(concat('upina-cloud.css'))
	.pipe(gulp.dest('./dist'));
});

gulp.task('watch', function() {
	gulp.watch('./src/*.scss', ['sass']);
});